package microvote2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import microvote2.config.WebConfig;
import microvote2.service.Microvote2Service;

@Configuration
@ComponentScan(basePackages = { "microvote2" })
@PropertySources({ @PropertySource(value = { "file:./config/application.properties" }, ignoreResourceNotFound = true),
        @PropertySource("classpath:application.properties") })
@EnableJpaRepositories(basePackages = "microvote2.dao")
@EnableTransactionManagement
public class App {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(App.class);
        new WebConfig(ctx.getBean(Microvote2Service.class));
        ctx.registerShutdownHook();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}