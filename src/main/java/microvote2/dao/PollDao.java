package microvote2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import microvote2.model.Poll;

@Repository
public interface PollDao extends CrudRepository<Poll, Long> {
}