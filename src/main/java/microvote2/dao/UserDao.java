package microvote2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import microvote2.model.User;

@Repository
public interface UserDao extends CrudRepository<User, Long> {
}