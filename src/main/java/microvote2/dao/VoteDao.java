package microvote2.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import microvote2.model.Poll;
import microvote2.model.User;
import microvote2.model.Vote;

@Repository
public interface VoteDao extends CrudRepository<Vote, Long> {
    List<Vote> findByUserAndPoll(User user, Poll poll);
}