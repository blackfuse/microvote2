package microvote2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import microvote2.model.Answer;

@Repository
public interface AnswerDao extends CrudRepository<Answer, Long> {
}