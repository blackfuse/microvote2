package microvote2.service;

import java.util.List;

import com.google.common.base.Strings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import microvote2.dao.AnswerDao;
import microvote2.dao.PollDao;
import microvote2.dao.UserDao;
import microvote2.dao.VoteDao;
import microvote2.model.Answer;
import microvote2.model.IllegalOwnerException;
import microvote2.model.NotFoundException;
import microvote2.model.Poll;
import microvote2.model.User;
import microvote2.model.Vote;
import microvote2.model.VotingRecord;

@Service
public class Microvote2Service {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private PollDao pollDao;

    @Autowired
    private AnswerDao answerDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private VoteDao voteDao;

    private static final int POLL_ID_HASH_LOC = 0;
    private static final int ANSWER_ID_HASH_LOC = 1;

    public User createUser() {
        User user = new User();
        return userDao.save(user);
    }

    public Poll getPoll(String token) throws NotFoundException {
        long pollId = tokenService.getIdFromToken(token, POLL_ID_HASH_LOC);
        return pollDao.findById(pollId).orElseThrow(() -> new NotFoundException(token));
    }

    public Poll createPoll(User user) {
        Poll poll = new Poll();
        poll.setOwner(user);
        poll = pollDao.save(poll);
        poll.setToken(tokenService.generateToken(poll.getId()));
        return pollDao.save(poll);
    }

    public Poll updatePoll(User user, Poll poll, String title, int maxVotes) throws IllegalOwnerException {
        checkPollOwnership(user, poll);
        poll.setTitle(title);
        poll.setMaxVotes(maxVotes);
        return pollDao.save(poll);
    }

    public void deletePoll(User user, Poll poll) throws IllegalOwnerException {
        checkPollOwnership(user, poll);
        pollDao.delete(poll);
    }

    public Answer getAnswer(String token) throws NotFoundException {
        long answerId = tokenService.getIdFromToken(token, ANSWER_ID_HASH_LOC);
        return answerDao.findById(answerId).orElseThrow(() -> new NotFoundException(token));
    }

    public Poll addAnswer(User user, Poll poll, String answerText) throws IllegalOwnerException {
        checkPollOwnership(user, poll);
        final long pollId = poll.getId();

        poll.addAnswer(answerText);
        poll = pollDao.save(poll);

        poll.getAnswers().stream().filter(q -> Strings.isNullOrEmpty(q.getToken()))
                .forEach(q -> q.setToken(tokenService.generateToken(pollId, q.getId())));
        return pollDao.save(poll);
    }

    public Poll deleteAnswer(User user, Answer answer) throws IllegalOwnerException {
        Poll poll = answer.getPoll();
        checkPollOwnership(user, poll);
        poll.getAnswers().remove(answer);
        return pollDao.save(poll);
    }

    public VotingRecord updateVote(User user, Answer answer, int numVotes) {
        Poll poll = answer.getPoll();
        List<Vote> userVotes = voteDao.findByUserAndPoll(user, poll);
        int userVoteTotal = userVotes.stream().map(Vote::getVoteCount).reduce((a, b) -> a + b).orElse(0);
        int remainingVotes = poll.getMaxVotes() - userVoteTotal;
        int votesToAdd = Math.min(numVotes, remainingVotes);
        Vote vote = userVotes.stream().filter(v -> v.getAnswer() == answer).findFirst().orElse(new Vote(user, answer));
        int newVoteCount = vote.getVoteCount() + votesToAdd;
        vote.setVoteCount(Math.max(newVoteCount, 0));
        voteDao.save(vote);
        userVotes = voteDao.findByUserAndPoll(user, poll);
        return new VotingRecord(userVotes);
    }

    private void checkPollOwnership(User user, Poll poll) throws IllegalOwnerException {
        if (poll.getOwner().getId() != user.getId())
            throw new IllegalOwnerException();
    }

}