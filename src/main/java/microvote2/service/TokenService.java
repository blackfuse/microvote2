package microvote2.service;

import org.hashids.Hashids;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TokenService {

    @Value("${hashId.salt:SomeSalt}")
    String salt;

    protected String generateToken(long... ids) {
        return hashids().encode(ids);
    }

    protected long getIdFromToken(String token, int idLoc) {
        try {
            return hashids().decode(token)[idLoc];
        }
        catch (ArrayIndexOutOfBoundsException e) {
            return -1;
        }
    }

    private Hashids hashids() {
        return new Hashids(salt, 5);
    }
}