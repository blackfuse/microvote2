package microvote2.config;

import static spark.Spark.*;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import microvote2.model.IllegalOwnerException;
import microvote2.model.NotFoundException;
import microvote2.model.Poll;
import microvote2.model.Answer;
import microvote2.model.User;
import microvote2.model.VotingRecord;
import microvote2.service.Microvote2Service;
import microvote2.util.JsonTransformer;
import spark.Request;

public class WebConfig {

    private Microvote2Service service;

    private JsonTransformer jsonTransformer;

    private Gson gson;

    final static Logger logger = LoggerFactory.getLogger(WebConfig.class);

    public WebConfig(Microvote2Service service) {
        this.service = service;
        this.gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        this.jsonTransformer = new JsonTransformer(gson);
        staticFileLocation("/public");
        routes();
    }

    private void routes() {
        path("/api", () -> {
            before("/*", (req, res) -> {
                User user = getUser(req);
                if (user == null) {
                    user = service.createUser();
                    req.session().attribute("user", user);
                }
            });
            path("/poll", () -> {

                get("/create", "application/json", (req, res) -> {
                    Poll poll = service.createPoll(getUser(req));
                    res.status(201);
                    return poll.getToken();
                });

                get("/:pollToken", "application/json", (req, res) -> {
                    String pollToken = req.params("pollToken");
                    return service.getPoll(pollToken);
                }, jsonTransformer);

                post("/:pollToken", "application/json", (req, res) -> {
                    Poll poll = service.getPoll(req.params("pollToken"));
                    JsonObject json = gson.fromJson(req.body(), JsonObject.class);
                    String title = json.get("title").getAsString();
                    int maxVotes = json.get("maxVotes").getAsInt();
                    poll = service.updatePoll(getUser(req), poll, title, maxVotes);
                    res.status(202);
                    return poll;
                }, jsonTransformer);

                delete("/:pollToken", "application/json", (req, res) -> {
                    String pollToken = req.params("pollToken");
                    Poll poll = service.getPoll(pollToken);
                    service.deletePoll(getUser(req), poll);
                    res.status(202);
                    return pollToken + " deleted";
                });

                post("/:pollToken/addAnswer", "application/json", (req, res) -> {
                    JsonObject json = gson.fromJson(req.body(), JsonObject.class);
                    Poll poll = service.getPoll(req.params("pollToken"));
                    String answerText = json.get("answer").getAsString();
                    poll = service.addAnswer(getUser(req), poll, answerText);
                    res.status(202);
                    return poll;
                }, jsonTransformer);

                delete("/:pollToken/answer/:answerToken", "application/json", (req, res) -> {
                    Answer answer = service.getAnswer(req.params("answerToken"));
                    Poll poll = service.deleteAnswer(getUser(req), answer);
                    res.status(202);
                    return poll;
                }, jsonTransformer);

                post("/:pollToken/answer/:answerToken/updateVote", "application/json", (req, res) -> {
                    Answer answer = service.getAnswer(req.params("answerToken"));
                    JsonObject json = gson.fromJson(req.body(), JsonObject.class);
                    int votes = json.get("votes").getAsInt();
                    VotingRecord votingRecord = service.updateVote(getUser(req), answer, votes);
                    res.status(202);
                    return votingRecord;
                }, jsonTransformer);
            });
        });

        exception(NotFoundException.class, (e, req, res) -> {
            res.status(404);
            res.body(e.getResourceId() + " not found");
        });

        exception(IllegalOwnerException.class, (e, req, res) -> {
            res.status(401);
            res.body("you do not own this");
        });

    }

    private User getUser(Request req) {
        return req.session().attribute("user");
    }

    public void stopServer() {
        stop();
    }
}