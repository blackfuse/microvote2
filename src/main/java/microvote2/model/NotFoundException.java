package microvote2.model;

public class NotFoundException extends Exception
{
    private static final long serialVersionUID = 1L;
	private String resourceId;

    public NotFoundException(String resourceId) {
        super();
        this.resourceId = resourceId;
    }

    public String getResourceId() {
        return resourceId;
    }
}