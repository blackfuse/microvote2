package microvote2.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "microvote_poll")
public class Poll {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Expose
	private String token;

	@ManyToOne
	@JoinColumn(name = "owner_id", foreignKey = @ForeignKey(name = "FK_poll_owner"))
	private User owner;

	@Expose
	private String title;

	@Expose
	private Integer maxVotes = 1;

	@Expose
	@OneToMany(mappedBy = "poll", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	List<Answer> answers = new ArrayList<>();


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public User getOwner() {
		return owner;
	}

	public Integer getMaxVotes() {
		return maxVotes;
	}

	public void setMaxVotes(Integer maxVotes) {
		this.maxVotes = maxVotes;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getToken() {
		return token;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public Answer addAnswer(String answerText) {
		Answer answer = new Answer();
		answer.setAnswer(answerText);
		answer.setPoll(this);
		answers.add(answer);
		return answer;
	}
	
}