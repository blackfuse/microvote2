package microvote2.model;

public class IllegalOwnerException extends Exception
{
    private static final long serialVersionUID = 1L;

    public IllegalOwnerException() {
        super();
    }
}