package microvote2.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name = "microvote_vote")
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "poll_id", foreignKey = @ForeignKey(name = "FK_vote_poll"))
    private Poll poll;

    @ManyToOne
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_vote_user"))
    private User user;

    @Expose
    @ManyToOne
    @JoinColumn(name = "answer_id", foreignKey = @ForeignKey(name = "FK_vote_answer"))
    private Answer answer;

    @Expose
    private int voteCount;

    public Vote() {
    }

    public Vote(User user, Answer answer) {
        this.user = user;
        this.answer = answer;
        this.poll = answer.getPoll();
    }

    public Poll getPoll(Poll poll) {
        return poll;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public int getVoteCount() {
        return voteCount;
    }

}