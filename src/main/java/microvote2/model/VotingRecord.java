package microvote2.model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.google.gson.annotations.Expose;

public class VotingRecord {

    @Expose
    private Map<String, Integer> voteMap;

    public VotingRecord(List<Vote> votes) {
        this.voteMap = votes.stream().collect(
                Collectors.groupingBy(v -> v.getAnswer().getToken(), Collectors.summingInt(Vote::getVoteCount)));
    }

    public Map<String, Integer> getVoteMap() {
        return voteMap;
    }
}