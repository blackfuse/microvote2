package microvote2.util;

import com.google.gson.Gson;

import spark.ResponseTransformer;

public class JsonTransformer implements ResponseTransformer {
	private Gson gson;

	public JsonTransformer(Gson gson) {
		super();
		this.gson = gson;
	}

	@Override
	public String render(Object model) throws Exception {
		return gson.toJson(model);
	}

}