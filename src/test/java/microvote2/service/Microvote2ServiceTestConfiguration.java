package microvote2.service;

import static org.mockito.Mockito.*;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import microvote2.dao.PollDao;
import microvote2.dao.AnswerDao;
import microvote2.dao.UserDao;
import microvote2.dao.VoteDao;

@Profile("test")
@Configuration
public class Microvote2ServiceTestConfiguration {

    @Bean
    public UserDao userDao() {
        return mock(UserDao.class);
    }

    @Bean
    public TokenService tokenService() {
        return mock(TokenService.class);
    }

    @Bean
    public PollDao pollDao() {
        return mock(PollDao.class);
    }

    @Bean
    public AnswerDao questionDao() {
        return mock(AnswerDao.class);
    }

    @Bean
    public VoteDao voteDao() {
        return mock(VoteDao.class);
    }
}