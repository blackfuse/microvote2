package microvote2.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import java.util.stream.IntStream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import microvote2.dao.AnswerDao;
import microvote2.dao.PollDao;
import microvote2.dao.UserDao;
import microvote2.dao.VoteDao;
import microvote2.model.Answer;
import microvote2.model.IllegalOwnerException;
import microvote2.model.NotFoundException;
import microvote2.model.Poll;
import microvote2.model.User;
import microvote2.model.Vote;

@ActiveProfiles("test")
@ContextConfiguration(classes = { Microvote2ServiceTestConfiguration.class, Microvote2Service.class })
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class Microvote2ServiceTest {

    @Autowired
    private Microvote2Service service;
    @Autowired
    private UserDao userDao;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private PollDao pollDao;
    @Autowired
    private AnswerDao answerDao;
    @Autowired
    private VoteDao voteDao;

    @Test
    public void testCreateUser() {
        when(userDao.save(any(User.class))).then(i -> {
            User saved = (User) i.getArgument(0);
            saved.setId(1L);
            return saved;
        });
        User user = service.createUser();
        assertThat(user).isNotNull();
        assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    public void testGetPoll() throws NotFoundException {
        String pollToken = "TOKEN";
        when(tokenService.getIdFromToken(eq(pollToken), eq(0))).thenReturn(1L);
        when(pollDao.findById(1L)).thenReturn(Optional.of(new Poll()));
        service.getPoll(pollToken);
        verify(pollDao).findById(1L);
    }

    @Test
    public void testGetPoll_NotFound() throws NotFoundException {
        String pollToken = "TOKEN";
        when(tokenService.getIdFromToken(eq(pollToken), eq(0))).thenReturn(2L);
        assertThatThrownBy(() -> {
            service.getPoll("OtherToken");
        }).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void testCreatePoll() {
        User user = getTestUser();
        String generatedToken = "TOKEN";

        when(tokenService.generateToken(1L)).thenReturn(generatedToken);

        when(pollDao.save(any(Poll.class))).then(i -> {
            Poll saved = (Poll) i.getArgument(0);
            assertThat(saved.getOwner()).isSameAs(user);
            saved.setId(1L);
            return saved;
        });

        Poll poll = service.createPoll(user);
        assertThat(poll.getToken()).isEqualTo(generatedToken);
    }

    @Test
    public void testUpdatePoll() throws Exception {
        when(pollDao.save(any(Poll.class))).then(i -> (Poll) i.getArgument(0));

        User user = getTestUser();
        Poll poll = getTestPoll(user);

        String title = "Poll Title";
        int maxVotes = 5;

        Poll updatedPoll = service.updatePoll(user, poll, title, maxVotes);
        assertThat(updatedPoll.getTitle()).isEqualTo(title);
        assertThat(updatedPoll.getMaxVotes()).isEqualTo(maxVotes);
    }

    @Test
    public void testUpdatePoll_NotOwned() {
        User user = getTestUser();
        Poll poll = getTestPoll(getTestUser(2L));
        assertThatThrownBy(() -> service.updatePoll(user, poll, null, 1)).isInstanceOf(IllegalOwnerException.class);
    }

    @Test
    public void testDeletePoll() throws IllegalOwnerException {
        User user = getTestUser();
        Poll poll = getTestPoll(user);

        service.deletePoll(user, poll);
        verify(pollDao).delete(poll);
    }

    @Test
    public void testDeletePoll_NotOwned() {
        User user = getTestUser();
        Poll poll = getTestPoll(getTestUser(2L));
        assertThatThrownBy(() -> service.deletePoll(user, poll)).isInstanceOf(IllegalOwnerException.class);
    }

    @Test
    public void testGetAnswer() throws Exception {
        String token = "TOKEN";
        when(tokenService.getIdFromToken(token, 1)).thenReturn(1L);
        when(answerDao.findById(1L)).thenReturn(Optional.of(new Answer()));
        service.getAnswer(token);
        verify(answerDao).findById(1L);
    }

    @Test
    public void testGetAnswer_NotFound() {
        String token = "TOKEN";
        when(tokenService.getIdFromToken((token), 0)).thenReturn(1L);
        assertThatThrownBy(() -> {
            service.getAnswer("OtherToken");
        }).isInstanceOf(NotFoundException.class);
    }

    @Test
    public void testAddAnswer() throws Exception {
        User user = getTestUser();
        Poll poll = getTestPoll(user);

        when(pollDao.save(any(Poll.class))).then(i -> {
            Poll savedPoll = (Poll) i.getArgument(0);
            IntStream.range(0, savedPoll.getAnswers().size())
                    .forEach(idx -> savedPoll.getAnswers().get(idx).setId(new Integer(idx + 1).longValue()));
            return savedPoll;
        });

        when(tokenService.generateToken(any())).thenReturn("TOKEN");

        poll = service.addAnswer(user, poll, "Test Answer");
        verify(pollDao, times(2)).save(poll);
        verify(tokenService).generateToken(poll.getId(), 1L);
        assertThat(poll.getAnswers()).allMatch(q -> q.getToken() == "TOKEN" && q.getAnswer() == "Test Answer");
    }

    @Test
    public void testAddAnswer_NotOwned() {
        User user = getTestUser();
        Poll poll = getTestPoll(getTestUser(2L));
        assertThatThrownBy(() -> service.addAnswer(user, poll, "TEXT")).isInstanceOf(IllegalOwnerException.class);
    }

    @Test
    public void testDeleteAnswer() throws Exception {
        User user = getTestUser();
        Poll poll = getTestPoll(user);
        poll.addAnswer("Other Test Answer");
        Answer answer = poll.addAnswer("Test Answer");

        when(pollDao.save(any(Poll.class))).then(i -> (Poll) i.getArgument(0));

        poll = service.deleteAnswer(user, answer);
        verify(pollDao).save(poll);
        assertThat(poll.getAnswers().size()).isEqualTo(1);
    }

    @Test
    public void testDeleteAnswer_NotOwned() {
        User user = getTestUser();
        Poll poll = getTestPoll(getTestUser(2L));
        assertThatThrownBy(() -> service.deleteAnswer(user, poll.addAnswer("test answer")))
                .isInstanceOf(IllegalOwnerException.class);
    }

    @Test
    public void testUpdateVote() {
        User user = getTestUser();
        Poll poll = getTestPoll(user);
        Answer answer = poll.addAnswer("Answer");
        answer.setToken("AToken");

        when(voteDao.findByUserAndPoll(user, poll)).thenReturn(Collections.emptyList());

        service.updateVote(user, answer, 3);
        verify(voteDao).save(argThat(
                (Vote vote) -> vote.getUser() == user && vote.getAnswer() == answer && vote.getVoteCount() == 1));
    }

    @Test
    public void testUpdateVote_UpdateExisting() {
        User user = getTestUser();
        Poll poll = getTestPoll(user);
        poll.setMaxVotes(3);

        Answer answer = poll.addAnswer("Answer");
        answer.setToken("AToken");

        Vote vote = new Vote(user, answer);
        vote.setVoteCount(2);
        when(voteDao.findByUserAndPoll(user, poll)).thenReturn(Collections.singletonList(vote));
        service.updateVote(user, answer, 3);
        verify(voteDao).save(argThat(
                (Vote saved) -> saved.getUser() == user && saved.getAnswer() == answer && saved.getVoteCount() == 3));
    }

    @Test
    public void testUpdateVote_UpdateExisting_Subtract() {
        User user = getTestUser();
        Poll poll = getTestPoll(user);
        poll.setMaxVotes(2);
        Answer answer = poll.addAnswer("Answer");
        answer.setToken("AToken");

        Vote vote = new Vote(user, answer);
        vote.setVoteCount(2);
        when(voteDao.findByUserAndPoll(user, poll)).thenReturn(Collections.singletonList(vote));

        service.updateVote(user, answer, -1);
        verify(voteDao).save(argThat(
                (Vote saved) -> saved.getUser() == user && saved.getAnswer() == answer && saved.getVoteCount() == 1));
    }

    // Helper Functions
    private Poll getTestPoll(User owner) {
        return getTestPoll(1L, owner);
    }

    private Poll getTestPoll(long id, User owner) {
        Poll poll = new Poll();
        poll.setId(1L);
        poll.setOwner(owner);
        return poll;
    }

    private User getTestUser() {
        return getTestUser(1L);
    }

    private User getTestUser(long id) {
        User user = new User();
        user.setId(id);
        return user;
    }

}