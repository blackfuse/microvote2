Feature: get a poll

Background:
* url 'http://localhost:4567'
* def poll = call read('poll-create.feature')

Scenario: succesfully get a poll
    Given path '/api/poll/' + poll.pollToken
    When method GET
    Then status 200
    And match $ == { token: '#(poll.pollToken)' , maxVotes: 1, answers: [] }

Scenario: fail getting a poll
    Given path '/api/poll/NOTAPOLL'
    When method GET
    Then status 404