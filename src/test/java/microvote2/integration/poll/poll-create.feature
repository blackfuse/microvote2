Feature: create a poll

Background:
* url 'http://localhost:4567'

Scenario: create a poll
    Given path '/api/poll/create'
    When method GET
    Then status 201
    And def pollToken = response
    And def cookies = responseCookies