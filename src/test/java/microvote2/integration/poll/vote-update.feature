Feature: update votes

Background:
* url 'http://localhost:4567'
* def poll = call read('answer-add.feature')

Scenario: update votes on answer
    Given path '/api/poll/' + poll.pollWithAnswers.token + '/answer/' + poll.answer1Token + '/updateVote'
    And request { votes: 2 }
    When method POST
    Then status 202
    And def voteTotal = response.voteMap[poll.answer1Token]
    And match voteTotal == 1    
    