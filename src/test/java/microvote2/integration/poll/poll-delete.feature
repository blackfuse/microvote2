Feature: update a poll

Background:
* url 'http://localhost:4567'
* def poll = call read('poll-create.feature')

Scenario: delete a poll
    * configure cookies = poll.cookies
    Given path '/api/poll/' + poll.pollToken
    When method DELETE
    Then status 202
    And match $ == poll.pollToken + " deleted"

Scenario: delete unowned poll
    * configure cookies = null
    Given path '/api/poll/' + poll.pollToken
    When method DELETE
    Then status 401

Scenario: delete nonexistant poll
    Given path '/api/poll/NOTAPOLL'
    When method DELETE
    Then status 404