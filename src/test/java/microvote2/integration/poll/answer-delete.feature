Feature: delete answer from poll

Background:
* url 'http://localhost:4567'
* def session = call read('answer-add.feature')

Scenario: delete answer on poll
    * configure cookies = session.userCookie
    Given path '/api/poll/' + session.pollWithAnswers.token + '/answer/' + session.pollWithAnswers.answers[0].token
    And method DELETE
    Then status 202
    And match response == { token: '#(session.pollWithAnswers.token)', maxVotes: '#(session.pollWithAnswers.maxVotes)', answers: ['#(session.pollWithAnswers.answers[1])']}