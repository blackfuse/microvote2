Feature: update a poll

Background:
* url 'http://localhost:4567'
* def poll = call read('poll-create.feature')

Scenario: update a poll
    * configure cookies = poll.cookies
    Given path '/api/poll/' + poll.pollToken
    And request { title: 'TITLE', maxVotes: 3 }
    When method POST
    Then status 202
    And match $ == { token: '#(poll.pollToken)', title: 'TITLE', maxVotes: 3, answers: [] }

Scenario: update unowned poll
    * configure cookies = null
    Given path '/api/poll/' + poll.pollToken
    And request { title: 'TITLE', maxVotes: 3 }
    When method POST
    Then status 401

Scenario: update nonexistant poll
    Given path '/api/poll/NOTAPOLL'
    And request { title: 'TITLE', maxVotes: 3 }
    When method POST
    Then status 404