Feature: add answers to poll

Background:
* url 'http://localhost:4567'
* def poll = call read('poll-create.feature')

Scenario: add answers to poll
    * configure cookies = poll.cookies
    Given path '/api/poll/' + poll.pollToken + '/addAnswer'
    And request { answer: 'Yes' }
    When method POST
    Then status 202
    And match response == { token: '#(poll.pollToken)' , maxVotes: 1, answers: [ { token: '#string', answer: 'Yes' } ] }
    And def answer1Token = response.answers[0].token

    Given path '/api/poll/' + poll.pollToken + '/addAnswer'
    And request { answer: 'No' }
    When method POST
    Then status 202
    And match response == { token: '#(poll.pollToken)' , maxVotes: 1, answers: [ { token: '#(answer1Token)', answer: 'Yes' }, { token: '#string', answer: 'No' } ] }
    And def pollWithAnswers = response
    And def userCookie = responseCookies

Scenario: add answer to unowned poll
    * configure cookies = null
    Given path '/api/poll/' + poll.pollToken + '/addAnswer'
    And request { answer: 'Yes' }
    When method POST
    Then status 401

Scenario: add answer to nonexistant poll
    Given path '/api/poll/NOTAPOLL/addAnswer'
    And request { answer: 'Yes' }
    When method POST
    Then status 404