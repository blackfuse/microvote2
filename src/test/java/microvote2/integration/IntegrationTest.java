package microvote2.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import cucumber.api.CucumberOptions;
import microvote2.App;
import microvote2.config.WebConfig;
import microvote2.service.Microvote2Service;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

@CucumberOptions(tags = { "~@ignore" })
public class IntegrationTest {
    private static AnnotationConfigApplicationContext ctx;
    private static WebConfig webConfig;

    @BeforeClass
    public static void before() {
        ctx = new AnnotationConfigApplicationContext(App.class);
        webConfig = new WebConfig(ctx.getBean(Microvote2Service.class));
    }

    @Test
    public void testParallel() {
        String karateOutputPath = "target/surefire-reports";
		KarateStats stats = CucumberRunner.parallel(getClass(), 5, karateOutputPath);
        generateReport(karateOutputPath);
        assertTrue("scenarios failed", stats.getFailCount() == 0);
    }

    private static void generateReport(String karateOutputPath) {
        Collection<File> jsonFiles = FileUtils.listFiles(new File(karateOutputPath), new String[] {"json"}, true);
        List<String> jsonPaths = new ArrayList<>(jsonFiles.size());
        jsonFiles.forEach(file -> jsonPaths.add(file.getAbsolutePath()));
        Configuration config = new Configuration(new File("target"), "demo");
        ReportBuilder reportBuilder = new ReportBuilder(jsonPaths, config);
        reportBuilder.generateReports();        
    }
    

    @AfterClass
    public static void after() {
        webConfig.stopServer();
        ctx.close();
    }
}